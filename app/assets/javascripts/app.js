(function() {
  angular.module('app', ['ui.router', 'smart-table'])
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });

      $stateProvider
        .state('project-new', {
          url: '/projects/new',
          templateUrl: '/templates/projects/create-edit.html',
          controller: 'ProjectNewCtrl'
        })
        .state('project-edit', {
          url: '/projects/:id/edit',
          templateUrl: '/templates/projects/create-edit.html',
          controller: 'ProjectEditCtrl',
          controllerAs: 'projEditCtrl'
        })
        .state('project-show', {
          url: '/projects/:id',
          templateUrl: '/templates/projects/show.html',
          controller: 'ProjectShowCtrl'
        })
        .state('project-list', {
          url: '/projects',
          templateUrl: '/templates/projects/index.html',
          controller: 'ProjectListCtrl',
          controllerAs: 'listCtrl'
        })

        .state('person-new', {
          url: '/persons/new',
          templateUrl: '/templates/persons/create-edit.html',
          controller: 'PersonNewCtrl'
        })
        .state('person-edit', {
          url: '/persons/:id/edit',
          templateUrl: '/templates/persons/create-edit.html',
          controller: 'PersonEditCtrl'
        })
        .state('person-show', {
          url: '/persons/:id',
          templateUrl: '/templates/persons/show.html',
          controller: 'PersonShowCtrl'
        })
        .state('person-list', {
          url: '/persons',
          templateUrl: '/templates/persons/index.html',
          controller: 'PersonListCtrl',
          controllerAs: 'listCtrl'
        });

        $urlRouterProvider.otherwise('/projects');
    }])

    .controller('ProjectListCtrl', ['$scope', '$http',
      function($scope, $http) {

        var listCtrl = this;

        $http.get('/api/v1/projects').then(function(resp) {
            listCtrl.projects = resp.data;
        });

        $scope.remove = function(project) {

          if (confirm('Are you sure you want to delete '+ project.title +'?')) {
            $http.delete('/api/v1/projects/'+ project.id).then(
              function() {
                listCtrl.projects.find(function(p, i) {
                  if (p === project) {
                    listCtrl.projects.splice(i, 1);
                  }
                });
              }
            );
          }

        };

      }
    ])

    .controller('ProjectEditCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        var vm = this;

        $http.get('/api/v1/projects/'+ $stateParams.id).then(function(resp) {
          var project = resp.data;

          $scope.project = {
            title: project.title,
            notes: project.notes
          };
        });

        $http.get('/api/v1/persons').then(function(resp) {
          $scope.persons = resp.data;
        });

        $scope.submit = function() {

          $http.put('/api/v1/projects/'+ $stateParams.id, $scope.project).then(
            function() {
              $state.go('project-list');
            }
          );
        };

        $scope.addRole = function() {
          console.log($scope.person);
          console.log($scope.role);

          $http.post('/api/v1/projects/'+ $stateParams.id +'/roles', $scope.project).then(
            function() {
              // Add to roles array
            }
          );

        };

      }
    ])
    .controller('ProjectShowCtrl', ['$scope', '$http', '$stateParams',
      function($scope, $http, $stateParams) {

        $http.get('/api/v1/projects/'+ $stateParams.id).then(function(resp) {
          $scope.project = resp.data;
        });

        $http.get('/api/v1/projects/'+ $stateParams.id + '/persons').then(function(resp) {
          $scope.persons = resp.data;
          console.log('folks: ', $scope.persons);
        });

      }
    ])
    .controller('ProjectNewCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        $scope.project = {};

        $scope.submit = function() {
          $http.post('/api/v1/projects', $scope.project).then(
            function() {
              $state.go('project-list');
            }
          )
        };

      }
    ])


    .controller('PersonListCtrl', ['$scope', '$http',
      function($scope, $http) {
        console.log('list');
        var listCtrl = this;

        $http.get('/api/v1/persons').then(function(resp) {
            listCtrl.persons = resp.data;
        });

        $scope.remove = function(person) {

          if (confirm('Are you sure you want to delete '+ person.first_name +' '+ person.last_name +'?')) {
            $http.delete('/api/v1/persons/'+ person.id).then(
              function() {
                listCtrl.persons.find(function(p, i) {
                  if (p === person) {
                    listCtrl.persons.splice(i, 1);
                  }
                });
              }
            );
          }

        };

      }
    ])
    .controller('PersonShowCtrl', ['$scope', '$http', '$stateParams',
      function($scope, $http, $stateParams) {

        $http.get('/api/v1/persons/'+ $stateParams.id).then(function(resp) {
          $scope.person = resp.data;
        });

      }
    ])
    .controller('PersonEditCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        $http.get('/api/v1/persons/'+ $stateParams.id).then(function(resp) {
          var person = resp.data;

          $scope.person = {
            first_name: person.first_name,
            last_name: person.last_name,
            notes: person.notes
          };
        });

        $scope.submit = function() {
          $http.put('/api/v1/persons/'+ $stateParams.id, $scope.person).then(
            function() {
              $state.go('person-list');
            }
          )
        };

      }
    ])
    .controller('PersonNewCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        $scope.person = {};

        $scope.submit = function() {
          console.log($scope.person);
          $http.post('/api/v1/persons', $scope.person).then(
            function() {
              $state.go('person-list');
            }
          )
        };

      }
    ])
})();
