class Person < ActiveRecord::Base
  has_many :roles
  has_many :projects, :through => :roles
  self.table_name = 'persons'
end
