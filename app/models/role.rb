class Role < ActiveRecord::Base
  belongs_to :person
  belongs_to :project

  enum role: {
    ACTOR: 0,
    DIRECTOR: 1
  }
end
