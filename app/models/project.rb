class Project < ActiveRecord::Base
  has_many :roles
  has_many :persons, :through => :roles
end
