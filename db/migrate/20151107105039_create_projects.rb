class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.date :release_date
      t.integer :release_date_accuracy
      t.text :notes
      t.timestamps null: false
    end
  end
end
