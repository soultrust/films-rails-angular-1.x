class CreateRoles < ActiveRecord::Migration

  def change
    create_table :roles do |t|
      t.references :person
      t.references :project
      t.integer :role
      t.timestamps null: false
    end
    add_index :roles, ['person_id', 'project_id']

  end

end
