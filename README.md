# Graphical interface for my films database #

My films database is comprised of films, cast and crew members. I built this GUI to keep track of films I watch and the people who work on them. My own personal IMDB.com.

Built in Angular 1.4 and Rails. I have stopped development on this project since I have decided to recreate it in Angular 2 in a new repo.